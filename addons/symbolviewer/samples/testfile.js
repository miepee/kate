class Class1 {
  constructor(param1, param2) {
  }
  foo1_in_Class1() {
  }
  static foo2_in_Class1() {
  }
}

function foo1(param1) {
}

class Class2 extends Class1 {
  constructor(param1, param2) {
    super(param1, param2)
  }
}
